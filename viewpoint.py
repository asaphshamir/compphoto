
from utils import *

def changeView(images, p, alpha, delta):
    """
    Calculates an image according to the line specified by p and alpha, within the volume created by images.
    :param images: a series of images
    :param p: the (x0, y0) of the virtual line
    :param alpha: the angle between the x axis and the virtual line
    :return: an image corresponding to the virtual line
    """
    tempDelta = delta
    tempImages = [np.copy(im) for im in images]
    if alpha == 0 or alpha == 180:
        return tempImages[int(np.round(p[1]))]

    N = len(tempImages)
    a = np.tan(alpha * np.pi / 180)
    n = tempImages[0].shape[1]

    flip = False
    x0, y0 = p
    y0 /= N

    # handles negative angles
    if a < 0:
        a = -a
        y0 = 1 - y0
        tempImages.reverse()
        if delta > (n / (a * N)):
            flip = True
        tempDelta = -delta

    b = y0 - a * x0
    y = np.arange(N) / (N - 1)
    x = np.round((y - b) / a * n).astype('i')
    pad = np.abs(np.round((tempDelta + n / (a * N)) / 2).astype('i'))

    strips = []
    for i in range(N):
        if x[i] < -pad or x[i] > n + pad:
            continue
        strips.append(tempImages[i][: , x[i] - pad: x[i] + pad + 1, ...])

    if flip:
        strips.reverse()
        im = np.hstack(strips)
    else:
        im = np.hstack(strips)

    return im
