import numpy as np
import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
from scipy.signal import convolve2d


def getBoundingBox(mask):
    """
    Find the bounding box of the hole in the mask.
    :param mask: binary image with 1's where the hole is, and 0's elsewhere.
    :return: 1) The offset of the bounding box top left corner from mask's top left corner.
             2) The shape of the bounding box.
    """
    ind = np.argwhere(mask == 1)
    offset = ind.min(axis=0)
    boxShape = ind.max(axis=0) - ind.min(axis=0) + [1,1]
    return offset, boxShape


def extractBoundedArea(im, boundingBox):
    """
    Extract the area in im specified by boundingBox.
    :param im: the image to extract the region from.
    :param boundingBox: a tuple of (offset, boxShape), the same as in getBoundingBox doc.
    :return: the extracted region.
    """
    offset, boxShape = boundingBox
    i, j = offset
    im = np.roll(np.roll(im, -i, axis=0), -j, axis=1)
    w, h = boxShape
    return im[:w, :h]


def buildNeighborsMatrix(mask, boundingBox):
    """
    Builds the num of neighbors sparse matrix of the pixels within the mask's hole bounding box.
    :param mask: the mask with the hole.
    :param boundingBox: the bounding box of the hole.
    :return: the num of neighbors matrix.
    """
    neighborMat = np.full(mask.shape, 4, dtype='f')
    neighborMat[(0,-1),:] = 3
    neighborMat[:,(0,-1)] = 3
    neighborMat[(0,0,-1,-1),(0,-1,0,-1)] = 2

    holeNumNeighbors = extractBoundedArea(neighborMat, boundingBox).flatten()
    return sp.spdiags(holeNumNeighbors, [0], holeNumNeighbors.size, holeNumNeighbors.size)


def buildSubtractionMatrix(mask, boundingBox):
    """
    Builds the sparse neighbors subtraction matrix (part of the laplacian matrix).
    :param mask: the mask with the hole.
    :param boundingBox: the bounding box of the hole.
    :return: the subtraction matrix.
    """
    holeMask = extractBoundedArea(mask, boundingBox)
    m, n = holeMask.shape

    indices = np.where(holeMask != 1)
    indices = np.ravel_multi_index(indices, holeMask.shape)

    belowDiag = np.array([-1] * ((m - 1) * n) + [0] * n)
    aboveDiag = np.flipud(belowDiag)
    rightDiag = np.array(([-1] * (n - 1) + [0]) * m)
    leftDiag  = np.flipud(rightDiag)

    shifts = [-n, -1, 1, n]
    data = (np.c_[belowDiag, rightDiag, leftDiag, aboveDiag]).T

    for i, shift in enumerate(shifts):
        diag = np.pad(data[i], np.abs(shift), 'constant')
        tInd = indices + np.abs(shift)
        diag[tInd] = 0
        diag[tInd + shift] = 0
        data[i,:] = diag[np.abs(shift):-np.abs(shift)]

    mat = sp.spdiags(data, shifts, m*n, m*n)

    return mat


def buildMatrix(mask):
    """
    Builds the laplacion matrix (left hand side of eq. 7 in the paper).
    :param mask: the mask of the hole.
    :return: the laplacion matrix.
    """
    boundingBox = getBoundingBox(mask)
    neighborsMat = buildNeighborsMatrix(mask, boundingBox)
    subtractMat = buildSubtractionMatrix(mask, boundingBox)
    return neighborsMat + subtractMat


def buildBoundaryCondition(target, mask, offset):
    """
    Builds the boundary condition term (first term in the right hand side of eq. 7 in the paper).
    :param target: the target image.
    :param mask: the mask of the hole.
    :param offset: the offset between target top left and mask top left corners.
    :return: the boundary condition vector.
    """
    padOffset = np.array(offset) - [1,1]

    if target.shape[0] == mask.shape[0]:
        target = np.pad(target, ((1,1),(0,0)), 'edge')

    if target.shape[1] == mask.shape[1]:
        target = np.pad(target, ((0,0),(1,1)), 'edge')

    croppedTarget = extractBoundedArea(target, (padOffset, np.array(mask.shape) + [2,2]))
    padMask = np.pad(1 - mask, ((1,1),(1,1)), 'constant', constant_values=1)
    w, h = padMask.shape
    I, J = np.meshgrid(np.arange(w), np.arange(h), indexing='ij')
    I = (0 <= (I + padOffset[0])) * (I < target.shape[0])
    J = (0 <= (J + padOffset[1])) * (J < target.shape[1])

    croppedTarget *= padMask * I * J
    boundaryImage = convolve2d(croppedTarget, np.array([[0, 1, 0], [1, 0, 1], [0, 1, 0]]), mode='valid')
    boundingBox = getBoundingBox(mask)

    return extractBoundedArea(boundaryImage, boundingBox).flatten()


def buildGradientCondition(source, target, mask, offset, guidance = None):
    """
    Builds the gradient condition term (second term in the right hand side of eq. 7 in the paper).
    :param source: the source image.
    :param mask: the mask of the hole.
    :return: the gradient condition vector.
    """

    if guidance is not None:
        gx, gy = guidance(source, target, mask, offset)
    else:
        gx = convolve2d(source, np.array([[-1, 1]]), mode='same')
        gy = convolve2d(source, np.array([[-1, 1]]).T, mode='same')


    dxgx = convolve2d(np.pad(gx, ((0,0), (0,1)), 'constant'), np.array([[0.5, 0.5]]), mode='valid') -\
           convolve2d(np.pad(gx, ((0,0), (1,0)), 'constant'), np.array([[0.5, 0.5]]), mode='valid')
    dygy = convolve2d(np.pad(gy, ((0,1), (0,0)), 'constant'), np.array([[0.5, 0.5]]).T, mode='valid') -\
           convolve2d(np.pad(gy, ((1,0), (0,0)), 'constant'), np.array([[0.5, 0.5]]).T, mode='valid')

    grad = extractBoundedArea(dxgx + dygy, getBoundingBox(mask))

    return grad.flatten()


def buildResultVector(source, target, mask, offset, guidance):
    """
    Builds the boundary condition plus divergence (right hand side of eq. 7 in the paper).
    :param source: the source image.
    :param target: the target image.
    :param mask: the mask of the hole.
    :param offset: the offset between target top left and mask top left corners.
    :return: the result vector
    """
    b = buildBoundaryCondition(target, mask, offset)
    g = buildGradientCondition(source, target, mask, offset, guidance)

    vec = b + g
    boundingBox = getBoundingBox(mask)
    holeMask = extractBoundedArea(mask, boundingBox)
    badIndices = np.where(holeMask != 1)
    goodIndices = np.where(holeMask == 1)
    badIndices = np.ravel_multi_index(badIndices, holeMask.shape)
    goodIndices = np.ravel_multi_index(goodIndices, holeMask.shape)
    vec[badIndices] = 0
    return vec, goodIndices


def seamlessCloningGrayscale(source, target, mask, offset, guidance = None):
    """
    Inserts the region of source specified by mask to target by offset. using gradient domain method.
    :param source: the source image.
    :param target: the target image.
    :param mask: the mask of the hole.
    :param offset: the offset between target top left and mask top left corners.
    :param guidance: a function creating the guidance field for the algorithm.
    :return: the outputted image.
    """
    A = buildMatrix(mask)
    b, indices = buildResultVector(source, target, mask, offset, guidance)
    res = spsolve(A, b)[indices]

    # res = np.clip(res, 0, 1)

    i, j = np.where(mask == 1)
    i += offset[0]
    j += offset[1]
    target[i,j] = res

    return target


def seamlessCloning(source, target, mask, offset, guidance = None):
    """
    Performs seamless cloning as in seamlessClaningGrayscale, on RGB images.
    :param source: the source image.
    :param target: the target image.
    :param mask: the mask of the hole.
    :param offset: the offset between target top left and mask top left corners.
    :param guidance: a function creating the guidance field for the algorithm.
    :return: the outputted image.
    """
    if source.ndim == 3:
        for i in range(3):
            target[:,:,i] = seamlessCloningGrayscale(source[:,:,i], target[:,:,i], mask, offset, guidance)

    else:
        target = seamlessCloningGrayscale(source, target, mask, offset, guidance)

    return target


def guidanceFuncOtsu(source, target, mask, offset):
    dx = convolve2d(source, np.array([[-1,1]]), mode='same')
    dy = convolve2d(source, np.array([[-1,1]]).T, mode='same')

    target = extractBoundedArea(target, (offset, mask.shape))

    dtargetx = convolve2d(target, np.array([[-1,1]]), mode='same')
    dtargety = convolve2d(target, np.array([[-1,1]]).T, mode='same')

    otsux = np.abs(dx) > np.abs(dtargetx)
    otsuy = np.abs(dy) > np.abs(dtargety)

    dx = dx * otsux + dtargetx * (1 - otsux)
    dy = dy * otsuy + dtargety * (1 - otsuy)

    return dx, dy


def shepardGrayscale(source, target, mask, offset, weightFunc):
    """
    Perform cloning through shepard's interpolation on grayscale image.
    :param source: the source image (same as Poisson)
    :param target: the target image (same as Poisson)
    :param mask: the mask (same as Poisson)
    :param offset: the offset (same as Poisson)
    :param weightFunc: a function defining the weight of a distance between pixels.
    :return: the output
    """
    boundMask = convolve2d(mask, np.array([[0, 1, 0], [1, 0, 1], [0, 1, 0]]), mode='same')
    boundMask[np.where(mask == 1)] = 0
    boundMask[np.where(boundMask != 0)] = 1

    bb = getBoundingBox(boundMask)
    croppedBoundMask = extractBoundedArea(boundMask, bb)
    croppedTarget = extractBoundedArea(extractBoundedArea(target, (offset, mask.shape)), bb)
    croppedSource = extractBoundedArea(source, bb)
    croppedBoundIm = (croppedTarget - croppedSource) * croppedBoundMask

    w, h = croppedBoundIm.shape
    i, j = np.meshgrid(np.arange(w), np.arange(h), indexing='ij')

    i -= w // 2
    j -= h // 2

    dists = np.sqrt(i ** 2 + j ** 2)
    func = np.vectorize(weightFunc)
    weights = func(dists)

    res = convolve2d(croppedBoundIm, weights, mode='same') / convolve2d(croppedBoundMask, weights, mode='same')
    off, shape = bb
    source[off[0]:off[0] + shape[0], off[1]:off[1] + shape[1]] += res
    i, j = np.where(mask == 1)
    target[i + offset[0], j + offset[1]] = source[i, j]

    return target


def shepard(source, target, mask, offset, weightFunc):
    """
    perform seamless cloning through shepard's interpolation an grayscale images.
    :param source: the source image (same as Poisson)
    :param target: the target image (same as Poisson)
    :param mask: the mask (same as Poisson)
    :param offset: the offset (same as Poisson)
    :param weightFunc: a function defining the weight of a distance between pixels.
    :return: the output
    """
    if source.ndim == 3:
        for i in range(3):
            target[:, :, i] = shepardGrayscale(source[:, :, i], target[:, :, i], mask, offset, weightFunc)
    else:
        target = shepardGrayscale(source, target, mask, offset, weightFunc)

    return target

