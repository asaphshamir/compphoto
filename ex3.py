
from tkinter import *
from tkinter import filedialog
from PIL import ImageTk, Image

from refocusing import *
from viewpoint import changeView

class gui(object):

    def __init__(self, root):
        """
        Initializes the GUI
        :param root: the main tkinter object
        """
        # user input vars
        self.depth = DoubleVar()
        self.x0 = DoubleVar()
        self.y0 = DoubleVar()
        self.alpha = DoubleVar()
        self.focusCheck = IntVar()
        self.viewCheck = IntVar()

        # additional vars
        self.imageDiretory = None
        self.delta = None
        self.ims = None
        self.output = None
        self.photo = None

        ## Widgets
        # menus
        self.menubar = Menu(root)
        self.fileMenu = Menu(self.menubar, tearoff=0)
        self.fileMenu.add_command(label='Open',
                                  command=self.loadImagesGui)
        self.fileMenu.add_separator()
        self.fileMenu.add_command(label='Exit',
                                  command=self.quitApp)
        self.modeMenu = Menu(self.menubar,
                             tearoff=0)
        self.modeMenu.add_checkbutton(label='Focus',
                                      command=self.switchToFocus,
                                      variable=self.focusCheck)
        self.modeMenu.add_checkbutton(label='Viewpoint',
                                      command=self.switchToView,
                                      variable=self.viewCheck)
        self.menubar.add_cascade(label='File',
                                 menu=self.fileMenu)
        self.menubar.add_cascade(label='Mode',
                                 menu=self.modeMenu)
        self.focusCheck.set(1)
        self.viewCheck.set(0)
        root.config(menu=self.menubar)

        # main window and canvas
        self.screen = Frame(root)
        self.canvas = Canvas(self.screen,
                             relief='groove',
                             bd = 2,
                             height=300,
                             width=1000,
                             bg='#000')
        self.horzScrollbar = Scrollbar(self.screen,
                                       orient=HORIZONTAL)
        self.vertScrollbar = Scrollbar(self.screen)

        self.screen.pack(side=TOP,
                         fill=BOTH,
                         expand=True)
        self.vertScrollbar.pack(side=RIGHT,
                                fill=Y)
        self.vertScrollbar.config(command=self.canvas.yview)
        self.canvas.pack(side=TOP,
                         fill=BOTH,
                         expand=True)
        self.horzScrollbar.pack(side=TOP,
                                fill=X)
        self.horzScrollbar.config(command=self.canvas.xview)
        self.canvas.config(xscrollcommand=self.horzScrollbar.set,
                           yscrollcommand=self.vertScrollbar.set)
        # This is what enables scrolling with the mouse:
        self.canvas.bind("<ButtonPress-1>", self.scroll_start)
        self.canvas.bind("<B1-Motion>", self.scroll_move)

        # refocusing tool
        self.focusFrame = Frame(root,
                                height=200,
                                bd =4,
                                relief='groove',
                                highlightthickness=2) # refocusing tool
        self.depthSlider = Scale(self.focusFrame,
                                 variable=self.depth,
                                 orient=HORIZONTAL,
                                 command=self.updateFocus,
                                 state=NORMAL,
                                 label='Distance of focal plane',
                                 length=400,
                                 repeatinterval=30,
                                 from_=.005,
                                 to_=2.2,
                                 resolution=.005,
                                 showvalue=0)
        self.nearButton = Button(self.focusFrame,
                                 text='near',
                                 command=self.nearFocus,
                                 state=NORMAL)
        self.midButton = Button(self.focusFrame,
                                text='mid',
                                command=self.midFocus,
                                state=NORMAL)
        self.farButton = Button(self.focusFrame,
                                text='far',
                                command=self.farFocus,
                                state=NORMAL)
        self.focusFrame.pack(side=LEFT,
                             fill=BOTH,
                             expand=True)
        self.depthSlider.grid(row=1, column=0)
        self.nearButton.grid(row=0, column=1)
        self.midButton.grid(row=1, column=1)
        self.farButton.grid(row=2, column=1)

        # viewpoint tool
        self.viewFrame = Frame(root,
                               height=200,
                               bd =4,
                               relief='groove',
                               highlightthickness=2)
        self.x0Slider = Scale(self.viewFrame,
                              variable = self.x0,
                              orient=HORIZONTAL,
                              state=DISABLED,
                              length=400,
                              from_=0,
                              to_=1,
                              resolution=0.01,
                              command=self.updateView,
                              label='x0')
        self.y0Slider = Scale(self.viewFrame,
                              variable = self.y0,
                              orient=HORIZONTAL,
                              state=DISABLED,
                              length=400,
                              from_=0,
                              to_=1,
                              resolution=0.05,
                              command=self.updateView,
                              label='y0')
        self.alphaSlider = Scale(self.viewFrame,
                                 variable = self.alpha,
                                 orient=HORIZONTAL,
                                 state=DISABLED,
                                 length=400,
                                 from_=-90,
                                 to_=90,
                                 resolution=1,
                                 command=self.updateView,
                                 label='angle')
        self.pushbroomButton = Button(self.viewFrame,
                                      text='Pushbroom',
                                      state=DISABLED,
                                      command=self.pushbroom)
        self.wideButton = Button(self.viewFrame,
                                 text='wide view',
                                 state=DISABLED,
                                 command=self.wide)
        self.leftButton = Button(self.viewFrame,
                                 text='left view',
                                 state=DISABLED,
                                 command=self.leftView)
        self.rightButton = Button(self.viewFrame,
                                  text='right view',
                                  state=DISABLED,
                                  command=self.rightView)
        self.forwardButton = Button(self.viewFrame,
                                  text='move forward',
                                  state=DISABLED,
                                  command=self.moveForward)
        self.backwardButton = Button(self.viewFrame,
                                  text='move backward',
                                  state=DISABLED,
                                  command=self.moveBackward)
        self.viewFrame.pack(side=RIGHT,
                            fill=BOTH,
                            expand=True)
        self.leftButton.grid(row=0, column=1)
        self.rightButton.grid(row=0, column=2)
        self.x0Slider.grid(row=0, column=0)
        self.y0Slider.grid(row=1, column=0)
        self.alphaSlider.grid(row=2, column=0)
        self.wideButton.grid(row=1, column=1)
        self.pushbroomButton.grid(row=1, column=2)
        self.forwardButton.grid(row=2, column=1)
        self.backwardButton.grid(row=2, column=2)


    def switchToView(self):
        """
        Switches the GUI to viewpoint mode
        """
        self.depthSlider['state'] = DISABLED
        self.nearButton['state'] = DISABLED
        self.midButton['state'] = DISABLED
        self.farButton['state'] = DISABLED
        self.x0Slider['state'] = NORMAL
        self.y0Slider['state'] = NORMAL
        self.alphaSlider['state'] = NORMAL
        self.pushbroomButton['state'] = NORMAL
        self.wideButton['state'] = NORMAL
        self.rightButton['state'] = NORMAL
        self.leftButton['state'] = NORMAL
        self.forwardButton['state'] = NORMAL
        self.backwardButton['state'] = NORMAL
        self.focusCheck.set(0)
        self.viewCheck.set(1)
        if self.ims is not None:
            self.initView()
            self.output = self.ims[0]
            self.displayOutput()


    def switchToFocus(self):
        """
        Switches the GUI to refocusing mode
        """
        self.x0Slider['state'] = DISABLED
        self.y0Slider['state'] = DISABLED
        self.alphaSlider['state'] = DISABLED
        self.pushbroomButton['state'] = DISABLED
        self.wideButton['state'] = DISABLED
        self.rightButton['state'] = DISABLED
        self.leftButton['state'] = DISABLED
        self.forwardButton['state'] = DISABLED
        self.backwardButton['state'] = DISABLED
        self.depthSlider['state'] = NORMAL
        self.nearButton['state'] = NORMAL
        self.midButton['state'] = NORMAL
        self.farButton['state'] = NORMAL
        self.focusCheck.set(1)
        self.viewCheck.set(0)
        if self.ims is not None:
            self.updateFocus(None)

    def displayOutput(self):
        """
        Tells the GUI to display the current image
        """
        if self.output.ndim == 3:
            im = Image.fromarray((self.output * 255).astype('uint8'), 'RGB')
        else:
            im = Image.fromarray((self.output * 255).astype('uint8'), 'L')
        self.photo = ImageTk.PhotoImage(image=im)
        self.canvas.create_image(0, 0, image=self.photo, anchor=NW)
        self.canvas.config(scrollregion=(0,0,self.output.shape[1], self.output.shape[0]))
        return


    def updateFocus(self, param):
        """
        Calculates a new image according to the current focus settings
        :param param: IGNORED
        """
        if self.ims is None:
            return
        self.output = refocus(self.ims, self.depth.get(), self.delta)
        self.displayOutput()
        return


    def updateView(self, param):
        """
        Calculates a new image according to the current viewpoint settings
        :param param: IGNORED
        """
        if self.ims is None:
            return
        self.output = changeView(self.ims, (self.x0.get(), self.y0.get()), self.alpha.get(), self.delta)
        self.displayOutput()
        return


    def quitApp(self):
        """
        Quits the GUI
        """
        sys.exit()

    def loadImagesGui(self):
        """
        Handles the Open command (loading a new directory)
        """
        self.imageDiretory = filedialog.askdirectory()
        self.ims = loadImages(self.imageDiretory, 1)
        u, v = imagesMotion(self.ims)
        self.delta = np.mean(u)
        self.ims = alignVertically(self.ims, v)
        self.initView()

    # part of mouse scrolling (taken as is from StackOverflow)
    def scroll_start(self, event):
        self.canvas.scan_mark(event.x, event.y)

    def scroll_move(self, event):
        self.canvas.scan_dragto(event.x, event.y, gain=1)


    def initView(self):
        """
        Initializes viewpoint tool scales
        """
        self.y0Slider['to_'] = len(self.ims) - 1
        self.y0.set(len(self.ims) // 2)
        self.x0.set(0.5)

    def leftView(self):
        """
        Moves the viewpoint a little to the left
        """
        if self.x0.get() > 0:
            self.x0.set(self.x0.get() - 0.05)
        self.updateView(0)

    def rightView(self):
        """
        Moves the viewpoint a little to the right
        """
        if self.x0.get() < 1:
            self.x0.set(self.x0.get() + 0.05)
        self.updateView(0)

    def pushbroom(self):
        """
        Moves the viewpoint to a pushbroom view
        """
        self.alpha.set(90)
        self.updateView(0)

    def wide(self):
        """
        Moves the viewpoint to the widest view
        """
        self.alpha.set(45)
        self.updateView(0)

    def moveForward(self):
        """
        Moves the viewpoint forwards a little
        """
        if self.alpha.get() >= -90 + 5:
            self.alpha.set(self.alpha.get() - 5)
        self.updateView(0)

    def moveBackward(self):
        """
        Moves the viewpoint backwards a little
        """
        if self.alpha.get() <= 90 - 5:
            self.alpha.set(self.alpha.get() + 5)
        self.updateView(0)

    def nearFocus(self):
        """
        Moves the focal plane close to the observer
        """
        self.depth.set(0.5)
        self.updateFocus(0)

    def midFocus(self):
        """
        Moves the focal plane to the middle
        """
        self.depth.set(1)
        self.updateFocus(0)

    def farFocus(self):
        """
        Moves the focal plane far from the observer
        """
        self.depth.set(2)
        self.updateFocus(0)



root = Tk()
gui(root)
root.mainloop()
