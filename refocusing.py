from utils import *
import numpy as np

def refocus(images, depth, delta):
    """
    Changes the focal plane depth.
    :param images: list of input images. (assumes images are from left to right)
    :param depth: the desired depth of focus
    :param delta: the shift in pixels between images
    :return: a single image refocused to the specified depth.
    """
    rolled = rollImages(images, depth, delta)
    tempimages = np.stack(rolled)
    return tempimages.mean(axis=0)

def rollImages(images, depth, delta):
    """
    Rolls all the images to align on the specified focal depth.
    :param images: the series of images
    :param depth: the depth on which to focus
    :param delta: the shift between images in pixels
    :return: the rolled images
    """
    n = len(images)
    midImage = n // 2
    rolled = [None] * n
    locs = np.arange(n) - midImage

    for i, loc in enumerate(locs):
        im = images[i]
        pixelShift = int(np.round(loc * delta / depth))
        im = np.roll(im, pixelShift, axis=1)
        if pixelShift < 0:
            im[:, pixelShift:] = 0
        else:
            im[:, :pixelShift] = 0

        rolled[i] = im

    return rolled


