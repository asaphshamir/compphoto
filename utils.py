import numpy as np
import matplotlib.pyplot as plt
import imghdr
import os

from scipy.misc import imread
from scipy.signal import convolve, convolve2d
from skimage.color import rgb2gray

def imReadAndConvert(filename, representation):
    """
    Reads an image file form filename.
    :param filename: the path of the image file.
    :param representation: if 0 use grayscale representation, otherwise uses RGB.
    :return: a numpy array of the image.
    """
    if representation == 0:
        im = imread(filename, True).astype(np.float32)
    else:
        im = imread(filename).astype(np.float32)
    if im.max() > 1.0:
        im /= 255

    return im


def showIm(im, representation=0):
    """
    Displays an image
    :param im: the image to display
    :param representation: 0 for grayscale, 1 for RGB
    """
    if representation == 0:
        if im.ndim == 3:
            im = rgb2gray(im)
        plt.imshow(im, cmap='gray')
    else:
        plt.imshow(im)

    plt.show()

def loadImages(folderPath, representation):
    """
    Reads all image files in a given directory.
    :param folderPath: the folder to read from.
    :param representation: 0 if images are to be grayscale and 1 if RGB.
    :return: a list of the images read.
    """
    images = []
    for f in sorted(os.listdir(folderPath)):
        filename = os.path.join(folderPath, f)
        if imghdr.what(filename) is not None:
            images.append(imReadAndConvert(filename, representation))
    return images


def ccMotion(I1, I2, initShifts=None):
    """
    Calculates the motion from I2 to I1, using cross correlation. (integer shifts only)
    :param I1: static image
    :param I2: moved image
    :param initShifts: initial guess to the shifts.
    :return: the shifts in rows and cols between I1 and I2.
    """
    if I1.ndim == 3:
        I1 = rgb2gray(I1)
    if I2.ndim == 3:
        I2 = rgb2gray(I2)

    I1 = I1 - I1.mean()
    I2 = I2 - I2.mean()

    m, n = I1.shape

    # if initial shifts are given, search +-3 away from shifts
    if initShifts:
        rows = np.arange(initShifts[0] - 3, initShifts[0] + 4).astype('i')
        cols = np.arange(initShifts[1] - 3, initShifts[1] + 4).astype('i')

    # else possible shifts are between -0.5 dim size to +0.5 dim size
    else:
        rows = np.arange(m).astype('i') - m // 2
        cols = np.arange(n).astype('i') - n // 2

    cc = -np.inf
    rowShift = None
    colShift = None
    for row in rows:
        for col in cols:
            temp = np.roll(np.roll(I2, row, axis=0), col, axis=1)
            if row < 0:
                if col < 0:
                    tempCC = (I1 * temp)[:row,:col].mean()
                else:
                    tempCC = (I1 * temp)[:row,col:].mean()
            else:
                if col < 0:
                    tempCC = (I1 * temp)[row:,:col].mean()
                else:
                    tempCC = (I1 * temp)[row:,col:].mean()
            if tempCC > cc:
                cc = tempCC
                rowShift = row
                colShift = col

    return rowShift, colShift


def ccMotionPyr(I1, I2, inShifts = None):
    """
    Calculates the shift between I1 and I2 using cross correlation and pyramids.
    :param I1:
    :param I2:
    :param inShifts:
    :return:
    """
    if I1.ndim == 3:
        I1 = rgb2gray(I1)
    if I2.ndim == 3:
        I2 = rgb2gray(I2)

    if inShifts is not None:
        shifts = ccMotion(I1, I2, inShifts)

    else:
        n = min(I1.shape)

        levels = int(np.floor(np.log2(n) - 4))

        p1 = gaussianPyramid(I1, levels, 5)
        p2 = gaussianPyramid(I2, levels, 5)

        shifts = None
        for i in reversed(range(levels)):
            shifts = ccMotion(p1[i], p2[i], shifts)
            if i > 0:
                shifts = [p * 2 for p in shifts]

    return shifts


def imagesMotion(images):
    """
    Calculates the shifts between each pair of images in a given image series.
    :param images: the image series
    :return: a tuple containing the horizontal and vertical shifts vectors.
    """
    u = np.zeros(len(images) - 1)
    v = np.zeros(len(images) - 1)
    shifts = None
    for i in range(len(images) - 1):
        shifts = ccMotionPyr(images[i], images[i+1], shifts)
        # shifts = ccMotionPyr(images[i], images[i+1], None)
        u[i] = shifts[1]
        v[i] = shifts[0]

    return u, v


def alignVertically(images, shifts):
    """
    Aligns a series of images vertically according to shifts.
    :param images: the images to be aligned
    :param shifts: the shifts between each pair of images
    :return: the images aligned
    """
    absShift = np.r_[0, np.cumsum(shifts)]
    padBelow = int(np.round(absShift.max()))
    padAbove = int(np.round(np.abs(absShift.min())))

    m = images[0].shape[0]

    if images[0].ndim == 3:
        padding = ((padAbove, padBelow), (0,0), (0,0))
    else:
        padding = ((padAbove, padBelow), (0,0))

    alligned = [np.pad(np.zeros_like(images[0]), padding, 'constant') for _ in range(len(images))]
    for i in range(len(images)):
        start = int(padAbove + absShift[i])
        alligned[i][start:start + m,...] = images[i]

    return alligned


def blurInImageSpace(inImage, kernelSize):
    """
	Blurs the input image using convolution
	:param inImage: the input image
	:param kernelSize: the size of the gaussian kernel to be used
	:return: the blurred image
	"""
    g = gaussianKernel(kernelSize)
    blurred = convolve2d(inImage, g, mode='same', boundary='symm')
    blurred = convolve2d(blurred, g.T, mode='same', boundary='symm')

    return blurred


def gaussianKernel(size):
    """
    Computes a row vector of binomial coefficients of length n.
    :param size: the length of the vector
    :return: row vector of binomial coefficients
    """
    ker = [1]
    for _ in range(size - 1):
        ker = convolve(ker, [1, 1])

    return (np.array(ker) / (2 ** (size - 1))).reshape([size, 1])


def gaussianCenterImage(kernelSize, imShape = None):
    """
    Builds an image with dims imShape with a 2d gaussian of size kernelSize in its center. If imShape is None, imShape = (kernelSize, kernelSize).
    :param kernelSize: the size of the gaussian.
    :param imShape: the dims of the output.
    :return: image with gaussian weights in the center.
    """
    if imShape is None:
        imShape = (kernelSize, kernelSize)
    g = gaussianKernel(kernelSize)
    gg = np.zeros(imShape)
    r_mid, c_mid = np.array(imShape) // 2
    margin = kernelSize // 2
    gg[r_mid - margin: r_mid + margin + 1, c_mid - margin: c_mid + margin + 1] = convolve2d(g, g.T)
    return gg


def blur(im, kernelSize):
    """
    Blurs the input image using FT.
    :param im: the input image.
    :param kernelSize: the size of the gaussian kernel to be used.
    :param rect: if true also multiply the FT by a rect(w) = 1 if abs(w) < pi, else 0.
    :return: the blurred image.
    """
    g2 = gaussianCenterImage(kernelSize, im.shape)
    G = np.fft.fft2(np.fft.ifftshift(g2))
    I = np.fft.fft2(im)

    return np.real(np.fft.ifft2(I * G))


def reduce(im, kerSize, factor=2):
    """
    Reduces the image by blurring it and sub sampling.
    :param im: the image.
    :param kerSize: size of the kernel to blur with.
    :param rect: if true, use rect while blurring.
    :param factor: the scaling factor.
    :return: reduced image.
    """
    im = blur(im, kerSize)
    h, w = im.shape
    rows = np.arange(0, h, factor)
    cols = np.arange(0, w, factor)
    i, j = [p.astype('int') for p in np.meshgrid(rows, cols, indexing='ij')]
    return im[i, j]


def gaussianPyramid(im, levels, filterSize, factor=2):
    """
    Creates a Gaussian pyramid of an image.
    :param im: the image.
    :param levels: the maximal number of levels in the pyramid.
    :param filterSize: the size of the gaussian filter to be used.
    :param rect: if true, use rect while blurring.
    :param factor: the scaling factor.
    :return: a Gaussian pyramid of the image and a row vector representing the gaussian kernel used.
    """
    pyr = (im,)
    for _ in range(levels):
        pyr += (reduce(pyr[-1], filterSize, factor),)

    return pyr

